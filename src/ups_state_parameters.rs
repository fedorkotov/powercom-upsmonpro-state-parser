use std::fmt;

use super::{MainsState, UPSMode};

/// UPS State 
#[derive(Debug, Default)]
pub struct UPSStateParameters {
    /// UPS operation mode
    pub mode: UPSMode,
    /// Utility power (mains) state
    pub mains_state: MainsState,
    /// RMS input voltage (Volts)
    pub voltage_in: u16,
    /// RMS output voltage (Volts)
    pub voltage_out: u16,
    /// Load level (%)
    pub load_percent: u8,
    /// output frequency (Hz)
    /// Line frequency when UPS is in passthrough, boost or buck mode
    /// or output frequency when on battery power
    pub frequency: u8,
    /// battery charge percent
    pub battery_charge_percent: u8,
    /// temperature (deg C). 
    /// Due to strange (to say the least) HTTP interface
    /// of UPSMON PRO temperature is unknown if
    /// UPS is on battery power. It uses the same parameter (5th line of plaintext ups.txt) for
    /// temperature and output frequency. How 5th line should be interpreted
    /// is determined by 10th line.
    pub temperature: Option<i8>,    
}

impl fmt::Display for UPSStateParameters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {        
        write!(
            f, 
            "UPS Mode: {}, Mains state: {}, Vin={} V, Vout={} V, f={} Hz, chg={} %, load={} %", 
            self.mode,
            self.mains_state,
            self.voltage_in, 
            self.voltage_out, 
            self.frequency, 
            self.battery_charge_percent,
            self.load_percent)?;
        if let Some(t) = self.temperature {
            write!(
                f,
                ", T={} C",
                t)?;
        }
        Ok(())
    }
}

impl UPSStateParameters{    
    #[allow(clippy::too_many_arguments)]
    pub fn new(mode: UPSMode,
               mains_state: MainsState,
               voltage_in: u16, 
               voltage_out: u16, 
               load_percent: u8,
               frequency: u8, 
               battery_charge_percent: u8,
               temperature: Option<i8>) -> UPSStateParameters {
        UPSStateParameters {
            mode,
            mains_state,
            voltage_in,
            voltage_out,
            load_percent,
            frequency,
            battery_charge_percent,
            temperature
        }
    }    
}