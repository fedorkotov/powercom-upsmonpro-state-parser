use std::fmt;
use std::fmt::{Display, Debug};

#[derive(Debug)]
#[derive(PartialEq)] 
pub enum MainsState{
    /// Mains voltage and frequency are within limits
    Ok,
    /// Mains voltage and/or frequency are out of limits
    Warning,
    /// Mains power failure
    Failure
}

impl Default for MainsState {
    fn default() -> MainsState {
        MainsState::Ok
    }
}

impl Display for MainsState{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Debug::fmt(self, f)
    }
}
