//! powercom-upsmonpro-state-parser is a parser of 
//! [POWERCOM](https://www.upspowercom.com/) UPS state provided as plain 
//! text by Windows versions of 
//! [UPSMON Pro](https://www.upspowercom.com/PRO-Windows.jsp) 
//! via HTTP (by default on port 8000) and as a plain text file (`C:\Program Files\UPSMONPRO\UPSMONWebSer\ups.txt`).
//!
//! This library is unofficial and is not endorsed by POWERCOM.
//! 
//! This simplified example program reads UPS state from 
//! hardcoded URL using [reqwest](https://crates.io/crates/reqwest) 
//! crate (blocking mode for simplicity), formats and writes it 
//! to standard output.
//! 
//! ```no_run
//! extern crate powercom_upsmonpro_state_parser;
//! extern crate reqwest;
//! 
//! use std::str::FromStr;
//! 
//! use powercom_upsmonpro_state_parser::{UPSState};
//! 
//! // replace 127.0.0.1 with actual
//! // ip of the machine where UPSMON PRO is installed.
//! const UPS_STATE_URL: &str = 
//!     "http://127.0.0.1:8000/ups.txt";
//! 
//! fn main() {
//!     let ups_state_str = 
//!         reqwest::blocking::get(UPS_STATE_URL)
//!         .unwrap()
//!         .text()
//!         .unwrap();
//! 
//!     let ups_state = 
//!         UPSState::from_str(
//!             ups_state_str.as_str())
//!         .unwrap();
//! 
//!     println!("{}", ups_state);
//! 
//!     if let UPSState::Connected(state) = ups_state         
//!     {
//!        if state.battery_charge_percent < 20
//!        {
//!           println!("WARNING: battery charge too low")
//!        }
//!     }
//! }
//! ```
//! Example output:
//! ```text
//! Connection status: Connected, UPS Mode: Normal, Mains state: Ok, Vin=228 V, Vout=228 V, f=50 Hz, chg=100 %, load=0 %, T=30 C
//! ```

#[cfg(test)]
#[macro_use] 
extern crate assert_matches;

mod errors;
pub use self::errors::{Error};  

mod ups_state;
mod mains_state;
mod ups_mode;
mod ups_state_parameters;
pub use self::ups_state_parameters::UPSStateParameters;
pub use self::mains_state::MainsState;
pub use self::ups_mode::UPSMode;
pub use self::ups_state::UPSState;    

mod parser;



mod tests;