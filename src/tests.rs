#[cfg(test)]
use std::str::FromStr;

#[cfg(test)]
use super::{
    Error, 
    UPSState, 
    UPSMode,
    MainsState};

#[test]
fn parse_correct_state_string() {
    let ups_txt ="250
200
0
50
30
99
1
1
1
2";
    let ups_state =  UPSState::from_str(ups_txt).unwrap();

    assert_matches!(
        ups_state,
        UPSState::Connected(_));

    if let UPSState::Connected(params) = ups_state {
        assert_eq!(params.voltage_in, 250);
        assert_eq!(params.voltage_out, 200);
        assert_eq!(params.frequency, 50);
        assert_eq!(params.temperature, Some(30));
        assert_eq!(params.mode, UPSMode::Normal);
        assert_eq!(params.mains_state, MainsState::Ok);
        assert_eq!(params.battery_charge_percent, 99);
        assert_eq!(params.load_percent, 0);
    }        
    else {
        panic!("Unexpected UPSState");
    }
}  

#[test]
fn format_correct_state_string() {
    let ups_txt ="250
200
0
50
30
99
1
1
1
2";
    let ups_state =  UPSState::from_str(ups_txt).unwrap();

    assert_matches!(
        ups_state,
        UPSState::Connected(_));
    
    assert_eq!(
        format!("{}",ups_state),
        "Connection status: Connected, UPS Mode: Normal, \
        Mains state: Ok, Vin=250 V, Vout=200 V, f=50 Hz, \
        chg=99 %, load=0 %, T=30 C")
}  

#[test]
fn parse_battery_power() {
    let ups_txt ="100
220
15
0
50
70
5
3
1
1";
    let ups_state =  UPSState::from_str(ups_txt).unwrap();

    assert_matches!(
        ups_state,
        UPSState::Connected(_));

    if let UPSState::Connected(params) = ups_state {
        assert_eq!(params.voltage_in, 100);
        assert_eq!(params.voltage_out, 220);
        assert_eq!(params.frequency, 50);
        assert_eq!(params.temperature, None);
        assert_eq!(params.mode, UPSMode::OnLine);
        assert_eq!(params.mains_state, MainsState::Failure);
        assert_eq!(params.battery_charge_percent, 70);
        assert_eq!(params.load_percent, 15);
    }
    else {
        panic!("Unexpected UPSState");
    }
}  

#[test]
fn format_battery_power() {
    let ups_txt ="100
220
15
0
50
70
5
3
1
1";
    let ups_state =  UPSState::from_str(ups_txt).unwrap();
    
    assert_eq!(
        format!("{}",ups_state),
        "Connection status: Connected, UPS Mode: OnLine, \
        Mains state: Failure, Vin=100 V, Vout=220 V, \
        f=50 Hz, chg=70 %, load=15 %")
}

#[test]
fn parse_no_usb_connection() {
    // the state really occurred for SPD-850 UPS
    let ups_txt ="0
0
0
0
0
0
1
1
3
2";
    let ups_state =  UPSState::from_str(ups_txt).unwrap();

    assert_eq!(
        format!("{}",ups_state),
        "Connection status: Disconnecting")
}  

#[test]
fn format_no_usb_connection() {
    let ups_txt ="0
0
0
0
0
0
1
1
3
2";
    let ups_state =  UPSState::from_str(ups_txt).unwrap();

    assert_matches!(
        ups_state,
        UPSState::Disconnecting);
} 

#[test]
fn parse_string_too_short() {
    let ups_txt ="250
200
0
50
30";
    assert_matches!(
        UPSState::from_str(ups_txt),
        Err(Error::UPSStateStringTooShort{ups_state: _}));
} 

#[test]
fn parse_charge_percent_off_range() {
    let ups_txt ="250
200
0
50
30
125
1
1
1
2";
    assert_matches!(
        UPSState::from_str(ups_txt),
        Err(Error::ChargePercentOutOfRange{charge_percent: _}));
} 

#[test]
fn parse_voltage_too_large() {
    let ups_txt ="250
123456
0
50
30
125
1
1
1
2";
    assert_matches!(
        UPSState::from_str(ups_txt),
        Err(Error::CanNotParseVoltage{line: _, err:_}));
} 

#[test]
fn parse_negative_voltage() {
    let ups_txt ="250
-30
0
50
30
125
1
1
1
2";
    assert_matches!(
        UPSState::from_str(ups_txt),
        Err(Error::CanNotParseVoltage{line: _, err:_}));
} 

#[test]
fn parse_temperature_too_large() {
    let ups_txt ="250
240
0
50
200
99
1
1
1
2";
    assert_matches!(
        UPSState::from_str(ups_txt),
        Err(Error::CanNotParseTemperature{line: _, err:_}));
} 