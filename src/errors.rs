use std::fmt;
use std::num::ParseIntError;
use std::error::Error as StdError;

#[derive(Debug)]
#[derive(PartialEq)]
pub enum Error {
    CanNotParseVoltage{line: String, err: ParseIntError}, 
    CanNotParseFrequency{line: String, err: ParseIntError}, 
    CanNotParseChargePercent{line: String, err: ParseIntError}, 
    CanNotParseLoadPercent{line: String, err: ParseIntError}, 
    CanNotParseUPSMode{line: String}, 
    CanNotParseMainsState{line: String}, 
    CanNotParseUPSConnectionStatus{line: String}, 
    CanNotParseTemperature{line: String, err: ParseIntError}, 
    ChargePercentOutOfRange{charge_percent: u8}, 
    UPSStateStringTooShort{ups_state: String}, 
    InternalError{description: String}
}

impl fmt::Display for Error{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::CanNotParseVoltage{line, err} => 
                write!(f, "'{}' is not a valid voltage: {}", line, err),
            Error::CanNotParseFrequency{line, err} => 
                write!(f, "'{}' is not a valid frequency: {}", line, err),
            Error::CanNotParseChargePercent{line, err} => 
                write!(f, "'{}' is not a valid battery charge percent: {}", line, err),
            Error::CanNotParseLoadPercent{line, err} => 
                write!(f, "'{}' is not a valid load percent: {}", line, err),
            Error::CanNotParseUPSMode{line} => 
                write!(f, "'{}' is not a valid UPS Mode", line),
            Error::CanNotParseMainsState{line} => 
                write!(f,"'{}' is not a valid mains state",line),
            Error::CanNotParseUPSConnectionStatus{line} => 
                write!(f,"'{}' is not a valid ups connection status", line),
            Error::CanNotParseTemperature{line, err} => 
                write!(f,"'{}' is not a valid temperature value: {}", line, err),
            Error::ChargePercentOutOfRange{charge_percent} => 
                write!(f,"Battery charge percent value {} is out of range", charge_percent),
            Error::UPSStateStringTooShort{ups_state} => 
                write!(f,"UPS state string is too short: {}", ups_state),
            Error::InternalError{description} => 
                write!(f,"Internal UPS state parser error: {}", description),
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match *self {
            Error::CanNotParseVoltage{line: _, ref err} => Some(err),
            Error::CanNotParseFrequency{line: _, ref err} => Some(err),
            Error::CanNotParseChargePercent{line: _, ref err} => Some(err),
            Error::CanNotParseLoadPercent{line: _, ref err} => Some(err),
            Error::CanNotParseUPSMode{line: _} => None, 
            Error::CanNotParseMainsState{line: _} => None, 
            Error::CanNotParseUPSConnectionStatus{line: _} => None,
            Error::CanNotParseTemperature{line: _, ref err} => Some(err),
            Error::ChargePercentOutOfRange{charge_percent: _} => None,
            Error::UPSStateStringTooShort{ups_state: _} => None,
            Error::InternalError{description: _} => None
        }
    }
}

