use std::fmt;
use std::fmt::{Display, Debug};

/// UPS operation mode
#[derive(Debug)]
#[derive(PartialEq)] 
pub enum UPSMode{
    /// Input voltage is present and is within limits. Load is powered 
    /// from mains. Battery is charging. Load is powered from mains 
    /// (for line-interactive type UPS).
    /// 
    /// *Important notice:* 
    /// 
    /// this mode should mean what is written above but 
    /// in my case UPSMON PRO reported Normal mode and small but nonzero 
    /// input voltage (~30 V in 220 V nominal country) for SPD-850 UPS 
    /// when in fact power plug was deliberately disconnected from 
    /// mains socket for testing purposes and UPS itself was beeping 
    /// to signal loss of external power.
    Normal,
    /// Input voltage is too low. 
    /// The UPS is increasing voltage without switching to battery power.
    BoostingVoltage,
    /// Input voltage is too high.
    /// The UPS is decreasing voltage without switching to battery power.
    BuckingVoltage,
    /// The UPS is in bypass mode. Load is connected directly to mains.
    /// Probably due to failure with the UPS or overload.
    Bypass,
    /// Can not say for sure what is this mode. Could not find official 
    /// source. UPSMON PRO never reported this mode for my UPS. 
    /// Probably the same as Normal but for OnLine (double conversion) 
    /// type UPS. 
    OnLine
}

impl Default for UPSMode {
    fn default() -> UPSMode {
        UPSMode::Normal
    }
}

impl Display for UPSMode{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Debug::fmt(self, f)
    }
}