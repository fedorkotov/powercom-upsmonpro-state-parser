use std::fmt;
use super::{UPSStateParameters};


#[derive(Debug)]
pub enum UPSState{
    /// UPSMON PRO is connected to UPS. 
    /// UPS state is known
    Connected(UPSStateParameters),
    /// UPSMON PRO is trying to establish connection to UPS. 
    /// UPS state is unknown.
    Connecting,
    /// UPSMON PRO is in the process of connection termination. 
    /// UPS state is unknown.
    Disconnecting,
    /// UPSMON PRO is not connected to UPS.
    /// UPS state is unknown.
    Disconnected
}

impl Default for UPSState {
    fn default() -> UPSState {
        UPSState::Disconnected
    }
}

impl fmt::Display for UPSState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            UPSState::Disconnected =>
                write!(
                    f, 
                    "Connection status: Disconnected"),
            UPSState::Connecting =>
                write!(
                    f, 
                    "Connection status: Connecting"),
            UPSState::Disconnecting =>
                write!(
                    f, 
                    "Connection status: Disconnecting"),
            UPSState::Connected(state_parameters) =>
                write!(
                    f, 
                    "Connection status: Connected, {}",
                    state_parameters),
            
        }        
    }
}
