[![pipeline][s1]][l1] [![MIT][s2]][l2] 

[s1]: https://gitlab.com/fedorkotov/powercom-upsmonpro-state-parser/badges/master/pipeline.svg
[l1]: https://gitlab.com/fedorkotov/powercom-upsmonpro-state-parser/-/pipelines/latest

[s2]: https://img.shields.io/badge/license-MIT-blue.svg
[l2]: https://gitlab.com/fedorkotov/powercom-upsmonpro-state-parser/-/blob/master/LICENSE


# powercom-upsmonpro-state-parser

[[_TOC_]]

## Overview
powercom-upsmonpro-state-parser is a parser of [POWERCOM](https://www.upspowercom.com/) UPS state provided as plain text by Windows versions of [UPSMON Pro](https://www.upspowercom.com/PRO-Windows.jsp) via HTTP (by default on port 8000) and as a plain text file (`C:\Program Files\UPSMONPRO\UPSMONWebSer\ups.txt`).

The parser is published on [crates.io](https://crates.io/crates/powercom-upsmonpro-state-parser). To use it, add the following to your `Cargo.toml` file:
```toml
[dependencies]
powercom-upsmonpro-state-parser = "1.0.0"
```

This library is unofficial and is not endorsed by POWERCOM.

## Examples

This simplified example program reads UPS state from hardcoded URL using [reqwest](https://crates.io/crates/reqwest) crate (blocking client for simplicity), formats and writes it to standard output.

```rust
extern crate powercom_upsmonpro_state_parser;
extern crate reqwest;

use std::str::FromStr;

use powercom_upsmonpro_state_parser::{UPSState};

// replace 127.0.0.1 with actual
// ip of the machine where UPSMON PRO is installed.
const UPS_STATE_URL: &str = 
    "http://127.0.0.1:8000/ups.txt";

fn main() {
    let ups_state_str = 
        reqwest::blocking::get(UPS_STATE_URL)
        .unwrap()
        .text()
        .unwrap();

    let ups_state = 
        UPSState::from_str(
            ups_state_str.as_str())
        .unwrap();

    println!("{}", ups_state);

    if let UPSState::Connected(state) = ups_state         
    {
        if state.battery_charge_percent < 20
        {
            println!("WARNING: battery charge too low")
        }
    }
}
```
Example output:
```
Connection status: Connected, UPS Mode: Normal, Mains state: Ok, Vin=228 V, Vout=228 V, f=50 Hz, chg=100 %, load=0 %, T=30 C
```
